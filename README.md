ics-ans-nextcloud
=================

Ansible playbook to install [Nextcloud](https://nextcloud.com).

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
