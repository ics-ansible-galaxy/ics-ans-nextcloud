import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('nextcloud')


def test_nextcloud_index(host):
    # This tests that traefik forwards traffic to the nextcloud web server
    # and that we can access the nextcloud login page
    cmd = host.command('curl -f -H Host:ics-ans-nextcloud-default -k -L https://localhost')
    assert cmd.rc == 0 or cmd.rc == 22
    assert '<a href="https://nextcloud.com"' in cmd.stdout or '400 Bad Request' in cmd.stderr
